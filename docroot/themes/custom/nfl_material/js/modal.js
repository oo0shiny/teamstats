(function ($, Drupal) {
    Drupal.behaviors.materialModal = {
        attach: function() {
            $('.modal-trigger').each(function () {
                var content = '<div id="modal1" class="modal">\n' +
                    '  <div class="modal-content">\n' +
                    '    <p>Click the sort buttons to sort the opposing teams by different stats. To see an in-depth analysis of a matchup, click on the team circle.</p>\n' +
                    '  </div>\n';
                var materializeModal = Drupal.dialog(content, {
                    title: 'Help',
                    resizable: false,
                    closeOnEscape: true,
                    height: 'auto',
                    width: 'auto',
                    beforeClose: false,
                    modal: true,
                    close: function (event) {
                        $(event.target).remove();
                    }

                });
                // Attach modal functionality to element on click.
                $(this).click(function () {
                    materializeModal.showModal();
                    // Add the ability to click outside the modal to close it.
                    $(document).find('.ui-widget-overlay').click(function () {
                        materializeModal.close();
                    });
                });
            });

        }
    }
})(jQuery, Drupal);