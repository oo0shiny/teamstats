(function ($) {
    // Init Isotope.
    Drupal.behaviors.isoSort = {
        attach: function() {
            var grid = $('.teams').isotope({
                itemSelector: '.team',
                layoutMode: 'fitRows',
                sortAscending: {
                    name: true,
                    wins: false,
                    losses: false,
                    pct: false
                },

                getSortData: {
                    name: '.name',
                    wins: '.wins parseInt',
                    losses: '.losses parseInt',
                    pct: '.pct parseFloat',
                }
            });
            // Sort items on button click
            $('.sort-by-button-group').on('click', 'button', function () {
                var sortByValue = $(this).attr('data-sort-by');
                grid.isotope({sortBy: sortByValue});
            });
        }
    }
})(jQuery);