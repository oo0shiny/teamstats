<?php

namespace Drupal\nfl_stats\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

class StatsController extends ControllerBase {

  public function matchup($team1, $team2) {
    $t1 = Node::load($team1);
    $t2 = Node::load($team2);
    $machine_name = strtolower(str_replace(" ","-", $t1->getTitle()));
    $t1machine_name = str_replace(".", "", $machine_name);
    $machine_name = strtolower(str_replace(" ","-", $t2->getTitle()));
    $t2machine_name = str_replace(".", "", $machine_name);

    $team_names = [
      $t1->id() => $t1->getTitle(),
      $t2->id() => $t2->getTitle(),
    ];

    $query = \Drupal::entityQuery('node');
    $home_teams = $query
      ->orConditionGroup()
      ->condition('field_home_team', $t1->id())
      ->condition('field_home_team', $t2->id());
    $away_teams = $query
      ->orConditionGroup()
      ->condition('field_away_team', $t1->id())
      ->condition('field_away_team', $t2->id());
    $game_list = $query
      ->condition('type', 'game')
      ->condition($home_teams)
      ->condition($away_teams)
      ->execute();

    arsort($game_list);
    $game_nodes = Node::loadMultiple($game_list);
    $stats_array = [
      'home' => [
        'w' => 0,
        'l' => 0,
        't' => 0
      ],
      'high_score' => [
        'points' => 0
      ],
      'low_score' => [
        'points' => 99
      ],
      'teams' => [
        $t1->id() => [
          'name' => $t1->getTitle(),
          'css' => $t1machine_name,
          'w' => 0,
          'l' => 0,
          't' => 0,
          'big_win' => [
            'diff' => 0
          ],
          'close_win' => [
            'diff' => 99
          ]
        ],
        $t2->id() => [
          'name' => $t2->getTitle(),
          'css' => $t2machine_name,
          'w' => 0,
          'l' => 0,
          't' => 0,
          'big_win' => [
            'diff' => 0
          ],
          'close_win' => [
            'diff' => 99
          ]
        ]
      ],
    ];
    $game_array = [];
    $i = 0;
    foreach ($game_nodes as $game) {
      $g = [
        'home' => $game->get('field_home_team')->target_id,
        'away' => $game->get('field_away_team')->target_id,
        'hscore' => $game->get('field_home_score')->value,
        'ascore' => $game->get('field_away_score')->value,
        'week' => $game->get('field_week')->value,
        'season' => $game->get('field_season')->value,
        'link' => '/node/' . $game->id(),
        'winner' => ''
      ];
      // Home team won.
      if ($g['hscore'] > $g['ascore']) {
        $diff = $g['hscore'] - $g['ascore'];
        $stats_array['teams'][$g['home']]['w'] += 1;
        $stats_array['teams'][$g['away']]['l'] += 1;
        $stats_array['home']['w'] += 1;
        $g['winner'] = 'home';

        // See if this is the biggest win for the home team.
        if ($diff >= $stats_array['teams'][$g['home']]['big_win']['diff']) {
          $stats_array['teams'][$g['home']]['big_win'] = [
            'diff' => $diff,
            'score' => $g['hscore'] . ' - ' . $g['ascore'],
            'week' => $g['week'],
            'season' => $g['season'],
            'link' => $g['link']
          ];
        }
        // See if this is the closest win for the home team.
        if ($diff <= $stats_array['teams'][$g['home']]['close_win']['diff']) {
          $stats_array['teams'][$g['home']]['close_win'] = [
            'diff' => $diff,
            'score' => $g['hscore'] . ' - ' . $g['ascore'],
            'week' => $g['week'],
            'season' => $g['season'],
            'link' => $g['link']
          ];
        }
      }
      // Away team won.
      elseif ($g['hscore'] < $g['ascore']) {
        $diff = $g['ascore'] - $g['hscore'];

        $stats_array['teams'][$g['away']]['w'] += 1;
        $stats_array['teams'][$g['home']]['l'] += 1;
        $stats_array['home']['l'] += 1;
        $g['winner'] = 'away';

        // See if this is the biggest win for the home team.
        if ($diff >= $stats_array['teams'][$g['away']]['big_win']['diff']) {
          $stats_array['teams'][$g['away']]['big_win'] = [
            'diff' => $diff,
            'score' => $g['ascore'] . ' - ' . $g['hscore'],
            'week' => $g['week'],
            'season' => $g['season'],
            'link' => $g['link']
          ];
        }
        // See if this is the closest win for the home team.
        if ($diff <= $stats_array['teams'][$g['away']]['close_win']['diff']) {
          $stats_array['teams'][$g['away']]['close_win'] = [
            'diff' => $diff,
            'score' => $g['ascore'] . ' - ' . $g['hscore'],
            'week' => $g['week'],
            'season' => $g['season'],
            'link' => $g['link']
          ];
        }
      }
      // Tie game.
      else {
        $stats_array['teams'][$g['home']]['t'] += 1;
        $stats_array['home']['t'] += 1;
        $stats_array['teams'][$g['away']]['t'] += 1;
      }

      // See if this is the highest/lowest scoring game.
      $points = $g['hscore'] + $g['ascore'];
      if ($points > $stats_array['high_score']['points']) {
        $stats_array['high_score'] = [
          'points' => $points,
          'score' => $g['hscore'] . ' - ' . $g['ascore'],
          'week' => $g['week'],
          'season' => $g['season'],
          'link' => $g['link']
        ];
      }
      elseif ($points < $stats_array['low_score']['points']) {
        $stats_array['low_score'] = [
          'points' => $points,
          'score' => $g['hscore'] . ' - ' . $g['ascore'],
          'week' => $g['week'],
          'season' => $g['season'],
          'link' => $g['link']
        ];
      }

      // Add game info to the list of all games.
      $game_array[$i] = [
        'home' => $team_names[$g['home']],
        'away' => $team_names[$g['away']],
        'hscore' => $g['hscore'],
        'ascore' => $g['ascore'],
        'week' => $g['week'],
        'season' => $g['season'],
        'winner' => $g['winner'],
        'link' => $g['link']
      ];
      $i++;
    }
    return [
      '#theme' => 'matchup_page',
      '#title' => $t1->getTitle() . ' vs ' . $t2->getTitle(),
      '#stats' => $stats_array,
      '#games' => $game_array
    ];
  }

}
