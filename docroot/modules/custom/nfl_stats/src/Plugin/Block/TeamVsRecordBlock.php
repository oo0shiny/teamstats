<?php

namespace Drupal\nfl_stats\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;

/**
 * Provides a Block listing all records vs other teams for a given team.
 *
 * @Block(
 *   id = "team_vs_block",
 *   admin_label = @Translation("Team Vs Block"),
 *   category = @Translation("NFL"),
 * )
 */


class TeamVsRecordBlock extends BlockBase {
  public $team;
  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
      $team_id = $node->id();
      $this->team = $team_id;

      $query = \Drupal::entityQuery('node');
      $group = $query
        ->orConditionGroup()
        ->condition('field_home_team', $node->id())
        ->condition('field_away_team', $node->id());
      $entity_ids = $query
        ->condition('type', 'game')
        ->condition($group)
        ->execute();

      arsort($entity_ids);
      $game_nodes = Node::loadMultiple($entity_ids);

      // Get all teams.
      $team_query = \Drupal::entityQuery('node');
      $team_nids = $team_query->condition('type', 'team')->execute();
      $teams = Node::loadMultiple($team_nids);

      $totals = [
        'w' => 0,
        'l' => 0,
        't' => 0
      ];
      $team_results = [];

      foreach ($teams as $team) {
        $machine_name = strtolower(str_replace(" ","-", $team->getTitle()));
        $machine_name = str_replace(".", "", $machine_name);
        $team_results[$team->getTitle()] = [
          'style' => $machine_name,
          'nid' => $team->id(),
          'g' => 0,
          'w' => 0,
          'l' => 0,
          't' => 0
        ];
      }
      foreach ($game_nodes as $game) {
        $home_team = $game->get('field_home_team')->target_id;
        $away_team = $game->get('field_away_team')->target_id;
        $home_score = $game->get('field_home_score')->value;
        $away_score = $game->get('field_away_score')->value;

        $result = $this->get_result($home_team, $home_score, $away_team, $away_score);
        $totals[$result] += 1;

        if ($this->team == $home_team) {
          $team_results[$teams[$away_team]->getTitle()][$result] += 1;
          $team_results[$teams[$away_team]->getTitle()]['g'] += 1;
        }
        else {
          $team_results[$teams[$home_team]->getTitle()][$result] += 1;
          $team_results[$teams[$home_team]->getTitle()]['g'] += 1;
        }

      }

    }
    return [
      '#theme' => 'nfl_vs_block',
      '#content' => $team_results,
      '#this_team' => $team_id
    ];
  }

  private function get_result($home, $hscore, $away, $ascore) {
    $result = 'w';
    if ($hscore == $ascore) {
      return $result = 't';
    }

    if ($home == $this->team && $hscore < $ascore) {
      return $result = 'l';
    }
    elseif ($away == $this->team && $hscore > $ascore) {
      return $result = 'l';
    }
    return $result;
  }

}