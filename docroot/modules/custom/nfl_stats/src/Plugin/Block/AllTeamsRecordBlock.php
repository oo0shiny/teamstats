<?php

namespace Drupal\nfl_stats\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Provides a Block displaying the overall record for every team.
 *
 * @Block(
 *   id = "all_teams_record_block",
 *   admin_label = @Translation("All Teams Record Block"),
 *   category = @Translation("NFL"),
 * )
 */


class AllTeamsRecordBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
      // Get all teams.
      $teams = [];
      $nids = \Drupal::entityQuery('node')->condition('type','team')->execute();
      $nodes =  Node::loadMultiple($nids);
      foreach ($nodes as $team) {
        $machine_name = strtolower(str_replace(" ","-", $team->getTitle()));
        $machine_name = str_replace(".", "", $machine_name);

        $teams[$team->id()] = [
          'name' => $team->getTitle(),
          'machine' => $machine_name,
          'w' => 0,
          'l' => 0,
          't' => 0,
          'pct' => .000
        ];
      }


      $nids = \Drupal::entityQuery('node')->condition('type','game')->execute();
      arsort($nids);
      $game_nodes =  Node::loadMultiple($nids);

      foreach ($game_nodes as $game) {
        $home_score = $game->get('field_home_score')->value;
        $away_score = $game->get('field_away_score')->value;

        $home_team = $game->get('field_home_team')->target_id;
        $away_team = $game->get('field_away_team')->target_id;

        $result = $this->get_result($game->get('field_home_team')->target_id, $home_score, $game->get('field_away_team')->target_id, $away_score);

      }
    }
    if (!empty($totals['t'])) {
      $wins = $totals['w'] + ($totals['t']/2);
    }
    else {
      $wins = $totals['w'];
    }
    $pct = $wins / ($totals['w'] + $totals['l'] + $totals['t']);
    return [
      '#theme' => 'total_record_block',
      '#team_name' => $node->getTitle(),
      '#team_css' => $machine_name,
      '#total' => $totals['w'] . ' - ' . $totals['l'] . ' - ' . $totals['t'],
      '#pct' => $pct
    ];

  }

  private function get_result($home, $hscore, $away, $ascore) {
    $result = 'w';
    if ($hscore == $ascore) {
      return $result = 't';
    }

    if ($home == $this->team && $hscore < $ascore) {
      return $result = 'l';
    }
    elseif ($away == $this->team && $hscore > $ascore) {
      return $result = 'l';
    }
    return $result;
  }

}