<?php

namespace Drupal\nfl_stats\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Provides a Block listing all games for a given team.
 *
 * @Block(
 *   id = "team_game_list",
 *   admin_label = @Translation("Team Game List"),
 *   category = @Translation("NFL"),
 * )
 */


class TeamGameList extends BlockBase {
  public $team;
  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
      $team_id = $node->id();
      $this->team = $team_id;

      $query = \Drupal::entityQuery('node');
      $group = $query
        ->orConditionGroup()
        ->condition('field_home_team', $node->id())
        ->condition('field_away_team', $node->id());
      $entity_ids = $query
        ->condition('type', 'game')
        ->condition($group)
        ->execute();

      arsort($entity_ids);
      $game_nodes = Node::loadMultiple($entity_ids);

      // Get all teams.
      $team_query = \Drupal::entityQuery('node');
      $team_nids = $team_query->condition('type', 'team')->execute();
      $teams = Node::loadMultiple($team_nids);

      $rows = [];
      $totals = [
        'w' => 0,
        'l' => 0,
        't' => 0
      ];
      $class = [
        'w' => 'win',
        'l' => 'loss',
        't' => 'tie'
      ];
      $sb_count = 0;
      foreach ($game_nodes as $game) {
        $home_team = $teams[$game->get('field_home_team')->target_id];
        $away_team = $teams[$game->get('field_away_team')->target_id];
        $home_score = $game->get('field_home_score')->value;
        $away_score = $game->get('field_away_score')->value;

        $result = $this->get_result($game->get('field_home_team')->target_id, $home_score, $game->get('field_away_team')->target_id, $away_score);
        $totals[$result] += 1;

        // Add some classes to the row.
        $classes[] = $class[$result];
        if (!is_numeric($game->get('field_week')->value)) {
          $classes[] = 'bold';
        }

        if ($result == 'w' && strtolower($game->get('field_week')->value) == 'superbowl') {
          $classes[] = 'superbowl';
          $sb_count++;
        }

        $rows[] = [
          'data' => [
            'season' => $game->get('field_season')->value,
            'week' => $game->get('field_week')->value,
            'home' => new FormattableMarkup('<a href=":link">@name</a>',
              [':link' => '/node/' . $home_team->id(),
                '@name' => $home_team->getTitle()
              ]),
            'score' => $home_score . ' - ' . $away_score,
            'away' => new FormattableMarkup('<a href=":link">@name</a>',
              [':link' => '/node/' . $away_team->id(),
                '@name' => $away_team->getTitle()
              ]),
            'link' => new FormattableMarkup('<a href=":link">@name</a>',
              [':link' => '/node/' . $game->id(),
                '@name' => 'View Game'
              ])
          ],
          'class' => [implode(' ', $classes)]
        ];
        // Empty the classes array so it doesn't carry over.
        $classes = [];
      }

    }
    $header = [
      ['data' => t('Season'), 'field' => 'season'],
      ['data' => t('Week'), 'field' => 'week'],
      ['data' => t('Home'), 'field' => 'home'],
      ['data' => t('Score'), 'field' => 'score'],
      ['data' => t('Away'), 'field' => 'away'],
      ['data' => t('Link'), 'field' => 'link'],
    ];
    $build['game_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows
    ];
    $build['trophies'] = $sb_count;
    return [
      '#theme' => 'team_game_list',
      '#content' => $build,
    ];

  }

  private function get_result($home, $hscore, $away, $ascore) {
    $result = 'w';
    if ($hscore == $ascore) {
      return $result = 't';
    }

    if ($home == $this->team && $hscore < $ascore) {
      return $result = 'l';
    }
    elseif ($away == $this->team && $hscore > $ascore) {
      return $result = 'l';
    }
    return $result;
  }

}