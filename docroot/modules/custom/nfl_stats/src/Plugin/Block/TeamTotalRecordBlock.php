<?php

namespace Drupal\nfl_stats\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;

/**
 * Provides a Block displaying the overall record for a given team.
 *
 * @Block(
 *   id = "total_record_block",
 *   admin_label = @Translation("Total Record Block"),
 *   category = @Translation("NFL"),
 * )
 */


class TeamTotalRecordBlock extends BlockBase {
  public $team;
  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
      $team_id = $node->id();
      $this->team = $team_id;

      // Get current team's info.
      $machine_name = strtolower(str_replace(" ","-", $node->getTitle()));
      $machine_name = str_replace(".", "", $machine_name);
      $color = '#ffffff';
      if ($node->hasField('field_primary_color')) {
        $color = $node->get('field_primary_color')->value;
      }
      $sb_wins = 0;
      if ($node->hasField('field_super_bowl_wins')) {
        $sb_wins = $node->get('field_super_bowl_wins')->value;
      }

      $query = \Drupal::entityQuery('node');
      $group = $query
        ->orConditionGroup()
        ->condition('field_home_team', $node->id())
        ->condition('field_away_team', $node->id());
      $entity_ids = $query
        ->condition('type', 'game')
        ->condition($group)
        ->execute();

      arsort($entity_ids);
      $game_nodes = Node::loadMultiple($entity_ids);

      $totals = [
        'w' => 0,
        'l' => 0,
        't' => 0
      ];
      foreach ($game_nodes as $game) {
        $home_score = $game->get('field_home_score')->value;
        $away_score = $game->get('field_away_score')->value;

        $result = $this->get_result($game->get('field_home_team')->target_id, $home_score, $game->get('field_away_team')->target_id, $away_score);
        $totals[$result] += 1;
      }
    }
    if (!empty($totals['t'])) {
      $wins = $totals['w'] + ($totals['t']/2);
    }
    else {
      $wins = $totals['w'];
    }




    $pct = $wins / ($totals['w'] + $totals['l'] + $totals['t']);
    return [
      '#theme' => 'total_record_block',
      '#team_name' => $node->getTitle(),
      '#team_css' => $machine_name,
      '#total' => $totals['w'] . ' - ' . $totals['l'] . ' - ' . $totals['t'],
      '#pct' => $pct,
      '#sbwins' => $sb_wins,
      '#color' => $color
    ];

  }

  private function get_result($home, $hscore, $away, $ascore) {
    $result = 'w';
    if ($hscore == $ascore) {
      return $result = 't';
    }

    if ($home == $this->team && $hscore < $ascore) {
      return $result = 'l';
    }
    elseif ($away == $this->team && $hscore > $ascore) {
      return $result = 'l';
    }
    return $result;
  }

}