<?php
/**
 * @file
 * Contains Drupal\nfl_recaps\Form\RecapBuilderForm.
 */
namespace Drupal\nfl_recaps\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

class RecapBuilderForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('nfl_recaps.adminsettings');

    $form['nfl_recaps_form'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Season'),
      '#description' => $this->t('Season used to create 32 Teams Recaps.'),
      '#default_value' => date('Y'),
    ];

    return parent::buildForm($form, $form_state);
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $season = $form_state->getValue('nfl_recaps_form');
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type', 'team');
    $entity_ids = $query->execute();
    $team_nodes = Node::loadMultiple($entity_ids);

    $exclude = [
      'Boston Patriots',
      'Houston Oilers',
      'St. Louis Cardinals',
      'Baltimore Colts',
      'Phoenix Cardinals',
      'St. Louis Rams',
      'Tennessee Oilers',
      'San Diego Chargers'
    ];
    foreach ($team_nodes as $team) {
      // Skip creating recaps for old (excluded) teams.
      if (in_array($team->getTitle(), $exclude)) {
        continue;
      }
      $recap_node = Node::create(['type' => '32_teams_recap']);
      $recap_node->set('field_recap_team', $team->id());
      $recap_node->set('field_season', $season);
      $recap_node->set('uid', 1);
      $recap_node->status = 0;
      $recap_node->enforceIsNew();
      $recap_node->save();
    }
    $messenger = \Drupal::messenger();
    $link = '/admin/config/32-team-recaps?season=' . $season;
    $msg = 'Recaps Created for the '.$season.' Season. <a href="'.$link.'">View them here</a>.';
    $messenger->addMessage($msg, $messenger::TYPE_STATUS);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'nfl_recaps.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nfl_recaps_build';
  }

}