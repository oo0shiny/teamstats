<?php
use Drupal\node\Entity\Node;
use \Drupal\Core\Url;

/**
 * Implements hook_entity_presave().
 */
function nfl_recaps_entity_presave(Drupal\Core\Entity\EntityInterface $entity) {
  if ($entity->bundle() == '32_teams_recap') {
    // If there's already a Weekly Recaps section, don't add a new one.
    $sections = $entity->get('field_recap_section')->referencedEntities();
    if (!empty($sections)) {
      foreach ($sections as $section) {
        if ($section->bundle() == 'weekly_recaps') {
          return;
        }
      }
    }
    $season = $entity->field_season->value;
    $team = $entity->field_recap_team->target_id;

    $author = $entity->getRevisionUserId();

    $game_query = \Drupal::entityQuery('node');
    $group = $game_query
      ->orConditionGroup()
      ->condition('field_home_team', $team)
      ->condition('field_away_team', $team);
    $entity_ids = $game_query
      ->condition('type', 'game')
      ->condition('field_season', $season)
      ->condition($group)
      ->execute();

    asort($entity_ids);
    $game_nodes = Node::loadMultiple($entity_ids);
    $recap_list = [];
    foreach ($game_nodes as $game) {
      $game_id = $game->id();
      $title = 'Game Recap: ' . $game->getTitle();
      // Create a new game recap.
      $node = Node::create(['type' => 'game_recap']);
      $node->set('title', $title);
      $node->set('field_game', $game_id);
      $node->set('uid', $author);
      $node->status = 1;
      $node->enforceIsNew();
      $node->save();

      $recap_list[] = $node->id();
    }

    // Now create the Weekly Recaps section to house the Game Recaps.
    $recap_node = Node::create(['type' => 'weekly_recaps']);
    $recap_node->set('field_recap_team', $team);
    $recap_node->set('uid', $author);
    $recap_node->status = 1;
    $recap_node->enforceIsNew();
    $recap_node->save();
    foreach ($recap_list as $recap) {
      $recap_node->field_recap->appendItem($recap);
    }
    $recap_node->save();
    $entity->field_recap_section->appendItem($recap_node->id());
  }
}

/**
 * Implements hook_page_attachments_alter() to add library code to specific pages.
 */
function nfl_recaps_preprocess_page(&$variables){
  if (isset($variables['node'])) {
    $node = $variables['node'];
    if($node && $node->bundle() == '32_teams_recap') {
      $page['#attached']['library'][] = 'nfl_recaps/expandgifs';
    }
  }
}

/**
 * Implements hook_page_attachments_alter() to add library code to specific pages.
 */
function nfl_recaps_page_attachments_alter(&$page){
  $path = \Drupal::service('path.current')->getPath();
  $alias = \Drupal::service('path.alias_manager')->getPathByAlias($path);

  $params = Url::fromUri("internal:" . $alias)->getRouteParameters();
  $entity_type = key($params);
  if ($entity_type == 'node'){
    $node = \Drupal::entityTypeManager()->getStorage($entity_type)->load($params[$entity_type]);
    if($node->bundle() == '32_teams_recap') {
      $page['#attached']['library'][] = 'nfl_recaps/expandgifs';
    }
  }
}


